ocr# Gsoc2022-tesseract-ocr
## My implementation of test for tesseract OCR


This script Python is supported for my progress of my project at Google Summer of Code 2022 including : 

- Specific test cases for each PSM.
- Pre-Processing methods to improve OCR accuracy.


### Project structure 

All test case is located in `.data`.

The source code is found in `.script/python`.

Text localization and detection by Tesserqct for each case found in folder `./results-ocr`.

### Report 

Explore my journey  [here](https://quochungtran.github.io/).



