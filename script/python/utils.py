import cv2
import os
import csv
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from skimage.filters import threshold_local
import numpy as np
import cv2


def img_box(results, 
            img,
            name_image):
    """
    This function takes three argument as
    input. it draw boxes on text area detected
    by Tesseract. it also writes resulted image to
    your local disk so that you can view it.
    :param img: image
    :param results: dictionary
    :param name_image: name_image
    :return: None
    """

    im = img.copy()
    for i in range(0, len(results["text"])):

        x = results["left"][i]
        y = results["top"][i]
        w = results["width"][i]
        h = results["height"][i]

        text = results["text"][i]
        conf = int(results["conf"][i])

        if conf > 20:
            text = "".join([c if ord(c) < 128 else "" for c in text]).strip()
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 200), 2)

        output_ocr_path = os.path.join('../../results-ocr', 'ocr_'+ name_image)
        cv2.imwrite(output_ocr_path, im)
        

    
def save_text_file(results) :
    """
    This function takes 
    Tesseract ocr results  
    as an argument
    :return: None
    """

    parse_text = []
    word_list  = []
    last_word  = []

    for word in results['text']:
        if word != '': 
            word_list.append(word)
            last_word = word

    if (last_word != '' and word == '') or (word == results['text'][-1]): 
        parse_text.append(word_list)
        word_list = []
    
    
    with open('result_text.txt', 'w', newline="") as file:
        csv.writer(file, delimiter=" ").writerows(parse_text)


def visualize(img):
    """
    This function takes 
    an image 
    as input
    :return: None
    """

    figure(figsize=(10, 30), 
           dpi=80)
    plt.imshow(img,cmap = 'gray')
    plt.show()



# ===========================================================================
# ============================ Functions to help ============================
# ===========================================================================

# 
def order_points(pts):
    """
    function to order points 
    to proper rectangle
    :pts:    4 any points  
    :return: 4 order points
    """

    rect = np.zeros((4, 2), dtype="float32")
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect

# 
def four_point_transform(image, pts):
    """
    function to transform image 
    to four points
    :image: any image 
    :pts: 4 order points
    :return: top_down view image
    """

    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array(
        [
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1],
        ],
        dtype="float32",
    )
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped

def findLargestCountours(cntList, cntWidths):
    """
    function to find largest contours
    :cntList: contours List 
    :cntWidths: contours widths
    :return: largest contours
    """
    
    newCntList   = []
    newCntWidths = []

    # finding 1st largest rectangle
    first_largest_cnt_pos = cntWidths.index(max(cntWidths))

    # adding it in new
    newCntList.append(cntList[first_largest_cnt_pos])
    newCntWidths.append(cntWidths[first_largest_cnt_pos])

    # removing it from old
    cntList.pop(first_largest_cnt_pos)
    cntWidths.pop(first_largest_cnt_pos)

    # finding second largest rectangle
    seccond_largest_cnt_pos = cntWidths.index(max(cntWidths))

    # adding it in new
    newCntList.append(cntList[seccond_largest_cnt_pos])
    newCntWidths.append(cntWidths[seccond_largest_cnt_pos])

    # removing it from old
    cntList.pop(seccond_largest_cnt_pos)
    cntWidths.pop(seccond_largest_cnt_pos)

    return newCntList, newCntWidths