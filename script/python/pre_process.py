from ast import withitem
from copy import copy
import cv2 
import pytesseract
import numpy as np
import imutils
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import csv
import os
import re
from utils import *


class PreProcessing_engine:

    def __init__(self, image) -> None:    

        self.image     = image 
        self.img_pre   = image 

    # set current image  
    def set_image(self, img):
        self.image = img

    # get current image
    def get_image(self): 
        return self.image
    
    # get current preprocessing image
    def get_image_proccesing(self):
        return self.img_pre 
    
    # get grayscale image
    def get_grayscale(self):
        return cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
    
    # noise removal
    def remove_noise(self):
        return cv2.GaussianBlur(self.image,(3,3),0)
    
    #thresholding
    def thresholding(self):
        #return cv2.adaptiveThreshold(self.image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C ,  cv2.THRESH_BINARY, 35, 2)
        #return cv2.adaptiveThreshold(self.image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2) 
        blur = cv2.GaussianBlur(self.image,(3,3),0)
        return  cv2.threshold(self.image ,127,255,cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    #dilation
    def dilate(self):
        kernel = np.ones((5,5),np.uint8)
        return cv2.dilate(self.image, kernel, iterations = 1)
    
    #erosion
    def erode(self):
        kernel = np.ones((5,5),np.uint8)
        return cv2.erode(self.image, kernel, iterations = 1)
    
    #opening - erosion followed by dilation
    def opening(self):
        kernel = np.ones((1,1),np.uint8)
        return cv2.morphologyEx(self.image, cv2.MORPH_OPEN, kernel)
    
    #canny edge detection
    def canny(self):
        return cv2.Canny(self.image, 100, 200)
    
    #skew correction
    def deskew(self):
        rotated = (self.image).copy()            
        gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)
        gray = cv2.bitwise_not(gray)
        # threshold the image, setting all foreground pixels to
        # 255 and all background pixels to 0
        thresh = cv2.threshold(gray, 0, 255,
    	     cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        coords = np.column_stack(np.where(thresh > 0))
        angle = cv2.minAreaRect(coords)[-1] 
        angle = -angle
        (h, w) = rotated.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(rotated, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)     

        return rotated

    def rotate_4_oritation(self):
        
        rotated = self.image.copy()
        (h, w) = rotated.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, 90, 1.0)
        rotated = cv2.warpAffine(rotated, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)     

        return rotated
        


    # remove shadow
    def remove_shadow(self):
        
        rgb_planes = cv2.split(self.image)

        result_planes = []
        result_norm_planes = []
        for plane in rgb_planes:
            dilated_img = cv2.dilate(plane, np.ones((3,3), np.uint8))
            bg_img = cv2.medianBlur(dilated_img, 21)
            diff_img = 255 - cv2.absdiff(plane, bg_img)
            norm_img = cv2.normalize(diff_img,None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
            result_planes.append(diff_img)
            result_norm_planes.append(norm_img)

            result = cv2.merge(result_planes)
            result_norm = cv2.merge(result_norm_planes)
        return result

    # correct distortion
    def correct_distortion(self) :

        ratio  = self.image.shape[0] / 500.0
        image  = imutils.resize( self.image, height = 500)

        # blur processing and edge-preserving filter 
        gray   = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        #gray   = cv2.GaussianBlur(gray, (3, 3), 0)
        gray   = cv2.bilateralFilter(gray, 11, 17, 17)  # 11  //TODO 11 FRO OFFLINE MAY NEED TO TUNE TO 5 FOR ONLINE
        gray   = cv2.medianBlur(gray, 5)
        edged  = cv2.Canny(gray, 30, 255)

        
        # find contours in the edged image, 
        # keep only the largest ones, 
        # and initialize our screen contour

        countours, hierarcy = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        orig = self.image.copy()
        # approximate the contour

        cnts = sorted(countours, key=cv2.contourArea, reverse=True)

        screenCntList = []
        scrWidths     = []

        for cnt in cnts:
            peri = cv2.arcLength(cnt, True) 
            # you want square but you got bad one so you need to approximate
            approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
            screenCnt = approx

            if len(screenCnt) == 4:
                (X, Y, W, H) = cv2.boundingRect(cnt)
                screenCntList.append(screenCnt)
                scrWidths.append(W)

        screenCntList, scrWidths = findLargestCountours(screenCntList, scrWidths)

        if not len(screenCntList) >= 2:  # there is no rectangle found
            print("No rectangle found")
        elif scrWidths[0] != scrWidths[1]:  # mismatch in rect
            print("Mismatch in rectangle")

        pts = screenCntList[0].reshape(4, 2)

        # Define our rectangle 
        rect = order_points(pts)

        warped = four_point_transform(orig, screenCntList[0].reshape(4, 2) * ratio)
        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)

        return warped
        
    

def Preprocessing_img_pipeline(img,
                               types):

    pre_eng = PreProcessing_engine(img)
    im = pre_eng.get_image()
    
    for type in types :
     
            if type ==  "gray" : 
                im  = pre_eng.get_grayscale()
                pre_eng.set_image(im)
            if type == "noise":    
                im = pre_eng.remove_noise()
                pre_eng.set_image(im)
            if type == "binary":
                im = pre_eng.thresholding()
                pre_eng.set_image(im)
            if type == "dilation":                
                im = pre_eng.dilate()
                pre_eng.set_image(im)
            if type == "erosion":
                im = pre_eng.erode()
                pre_eng.set_image(im)
            if type == "opening": 
                im = pre_eng.opening()
                pre_eng.set_image(im)
            if type == "canny": 
                im = pre_eng.canny()  
                pre_eng.set_image(im)
            if type == "deskew" : 
                im = pre_eng.deskew()  
                pre_eng.set_image(im)
            if type == "scanner":                 
                im = pre_eng.correct_distortion()  
                pre_eng.set_image(im)
            if type == "remove_shadow":
                im = pre_eng.remove_shadow()
                pre_eng.set_image(im)
    
    #visualize(pre_eng.get_image())
    return pre_eng.get_image()


